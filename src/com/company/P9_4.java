package com.company;

/**
 * Hung Nguyen
 * 013626210
 * This program asks user to check appointment or create appointments
 */

import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * superclass to create appointments
 */
class Appointments{
    //store appointment info
    private int mDate, mMonth, mYear;
    private char cType;
    //store appointment description
    private String mDescription;


    //constructor for appointments
    public Appointments(int nYear, int nMonth, int nDay, String tempDes){
        this.mDate = nDay;
        this.mMonth = nMonth;
        this.mYear = nYear;
        this.mDescription = tempDes;
    }

    /**
     * method to check if appointment occurs on date
     * @param year
     * @param month
     * @param day
     * @return true/false
     */
    public boolean occursOn(int year, int month, int day) {
        if ((this.mDate == day) && (this.mMonth == month) && (this.mYear == year)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * setter for date
     * @param nDate
     */
    public void setmDate(int nDate) {
        this.mDate = nDate;
    }

    /**
     * getter for date
     * @return mDate
     */
    public int getmDate() {
        return this.mDate;
    }

    /**
     * setter for month
     * @param nMonth
     */
    public void setmMonth(int nMonth){
        this.mMonth = nMonth;
    }

    /**
     * getter for month
     * @return mMonth
     */
    public int getmMonth(){
        return this.mMonth;
    }

    /**
     * setter for year
     * @param nYear
     */
    public void setmYear(int nYear){
        this.mYear = nYear;
    }

    /**
     * getter for year
     * @return mYear
     */
    public int getmYear(){
        return this.mYear;
    }

    /**
     * setter for description
     * @param strDescription
     */
    public void setmDescription(String strDescription){
        this.mDescription = strDescription;
    }
    public String getmDescription(String strDescription){
        return this.mDescription;
    }

    /**
     * getter for appointment
     * @return strReturn
     */
    public String getAppointment() {
        String strReturn = this.mDescription + " - " + this.mDate + "/" + this.mMonth + "/" + this.mYear;
        return strReturn;
    }

//    public ArrayList<Appointments> getAppLists() {
//        return this.appLists;
//    }
}

/**
 * Inherits appointment as a one time appointment
 */
class Onetimes extends Appointments {
    //passes user value to Appointment
    public Onetimes( int nYear, int nMonth, int nDay, String tempDes) {
        super( nYear, nMonth, nDay, tempDes);
    }

    /**
     * override method from Appointment
     * @return strOneTime
     */
    public String getAppointment() {
        String strOneTime = "oneTime appoitnment: " + super.getAppointment();
        return strOneTime;
    }
}

/**
 * inherits Appointment as daily appointment
 */
class Dailies extends Appointments {
    //passes user input to Daily
    public Dailies( int nYear, int nMonth, int nDay, String tempDes) {
        super( nYear, nMonth, nDay, tempDes);
    }

    @Override
    /**
     * overrides occursOn and check if input is the same or after
     * appointment
     * @param year,month,day
     * @return bReturn
     */
    public boolean occursOn(int year, int month, int day) {
        boolean bReturn = false;
        //create instances of LocalDateTime to compare time
        LocalDateTime a = LocalDateTime.of(year, month, day, 12, 00);
        LocalDateTime b = LocalDateTime.of(this.getmYear(), this.getmMonth(), this.getmDate(), 12, 00);
        //if userInput > daily or if equal date
        if(a.isAfter(b) || a.isEqual(b)){
            bReturn = true;
        }
        else{
            bReturn = false;
        }

        return bReturn;
    }
    @Override
    /**
     * overirdes getAppointment and return daily appointment string
     * @return strDaily
     */
    public String getAppointment() {
        String strDaily = "Daily appoitnment:  " + super.getAppointment();
        return strDaily;
    }
}

/**
 * extends Appointment as monthly appointment
 */
class Monthlies extends Appointments{
    public Monthlies( int nYear, int nMonth, int nDay, String tempDes) {
        super(nYear,nMonth,nDay,tempDes);
    }

    /**
     * Check if monthly is the same day as user input
     * @param year
     * @param month
     * @param day
     * @return occursOn
     */
    @Override
    public boolean occursOn(int year, int month, int day) {
        if ((this.getmMonth() <= month && this.getmDate()== day && this.getmYear() <= year)
                ||(this.getmMonth()>= month && this.getmDate() == day && this.getmYear() < year )){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return monthly appointment string
     * @return strMonthly
     */
    @Override
    public String getAppointment() {
        String strMonthly = "Monthly appoitnment: " + super.getAppointment();
        return strMonthly;
    }

}

/**
 * asks user to enter appointment
 */
public class P9_4 {
    public static final Scanner in = new Scanner(System.in);

    /**
     * check if string has letters/special characters
     *
     * @param strTemp
     * @return ArrayList
     */
    public static ArrayList<Character> CheckValid(String strTemp) {
        ArrayList<Character> arrChars = new ArrayList<Character>();
        boolean bRep = true;
        boolean flag = true;

        //loop through string to see if it has illegal inputs
        for (int i = 0; i < strTemp.length(); i++) {
            flag = Character.isDigit(strTemp.charAt(i));
            if (flag == true) {
                i++;
            } else {
                arrChars.add(strTemp.charAt(i));
            }
        }
        return arrChars;
    }

    /**
     * Check if user input of day is valid
     *
     * @return nDay
     */
    public static int CheckDay() {
        int nDay = 0;
        int nCount = 0;
        //create a pattern instance
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        //check if input is valid
        do {
            if (nCount >= 1) {

                System.out.println(String.format("%s is not valid", nDay));
            }
            System.out.print("Enter day: ");
            //while userinput is wrong
            while (!in.hasNextInt()) {
                String strTemp = in.next();
                Matcher m = p.matcher(strTemp);
                boolean b = m.find();
                //if input is invalid
                if (b) {
                    System.out.println(String.format("Special character %s is not valid", strTemp));
                    System.out.print("Enter day: ");
                } //if there is no special character
                else {
                    System.out.println(String.format("Letters are not valid: %s", strTemp));
                    System.out.print("Enter day: ");
                }
            }
            nDay = in.nextInt();
            nCount++;
        } while (nDay < 0 || nDay > 32);

        return nDay;

    }

    /**
     * Checking if user input of month is valid
     *
     * @return nMonth
     */
    public static int CheckMonth() {
        int nCount = 0;
        int nMonth = 0;
        //create pattern instance
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        //check if user input of month is valid
        do {
            if (nCount >= 1) {
                System.out.println(String.format("%s is not valid", nMonth));
            }
            System.out.print("Enter Month: ");
            //while user input is invalid
            while (!in.hasNextInt()) {
                String strTemp = in.next();
                Matcher m = p.matcher(strTemp);
                boolean b = m.find();
                //if input contains special characters
                if (b) {
                    System.out.println(String.format("Special character %s is not valid", strTemp));
                    System.out.print("Enter Month: ");
                } else { //if input contains letters
                    System.out.println(String.format("Letters are not valid: %s", strTemp));
                    System.out.print("Enter Month: ");
                }
            }
            nMonth = in.nextInt();
            nCount++;
        } while (nMonth < 0 || nMonth > 12);

        return nMonth;

    }

    /**
     * create new substring of user input date string
     * @param strTemp
     * @return strTemp
     */
    public static String moveStringIndex(String strTemp) {
        //create a new substring after -
        strTemp = strTemp.substring(strTemp.indexOf('-') + 1);
        return strTemp;
    }

    /**
     * asks for user input
     *
     * @param args
     */
    public static void main(String[] args) {


        boolean bRepeat = true;
        //create array list of appointments
        ArrayList<Appointments> appLists = new ArrayList<Appointments>();
        //store user input
        char cInput = 1;
        //while user doesn't enter q
        while (cInput != 'Q') {
            //while(cInput != 'Q' || cInput != 'A' || cInput)
            System.out.print("Select an option: A for add an appointment, C for checking, Q to quit: ");
            //read user input and store the first uppercase character
            String strInput = in.next();
            cInput = strInput.charAt(0);
            cInput = Character.toUpperCase(cInput);
            //if user choose A
            if (cInput == 'A') {
                //asks user to enter choice
                System.out.print("Enter the type (O-Onetime, D-Daily, or M-Monthly):");
                char cType = Character.toUpperCase(in.next().charAt(0));

                System.out.print("Enter the date (yyyy-mm-dd): ");
                //create an instance of DateTimeFormatter to change date values from yyyy/mm/d to yyyy-mm-dd
                DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy/MM/d");
                strInput = in.next();
                LocalDate localDate = LocalDate.parse(strInput, format);

                //store formatted date into strInput
                strInput = String.valueOf(localDate);
                //store year into nYear
                int nYear = Integer.parseInt(strInput.substring(0, strInput.indexOf('-')));
                //find next numbers after -
                strInput = moveStringIndex(strInput);
                //store into month
                int nMonth = Integer.parseInt(strInput.substring(0, strInput.indexOf('-')));
                //find next numbers after -
                strInput = moveStringIndex(strInput);
                //store number after last -
                int nDay = Integer.parseInt(strInput);

                System.out.print("Enter the description: ");
                String strDescription = in.next();
                //Check which type of appointment user want, then append into list
                if (cType == 'O') {
                    appLists.add(new Onetimes(nYear, nMonth, nDay, strDescription));
                } else if (cType == 'D') {
                    appLists.add(new Dailies(nYear, nMonth, nDay, strDescription));

                } else if (cType == 'M') {
                    appLists.add(new Monthlies(nYear, nMonth, nDay, strDescription));
                }
                System.out.println("Added appointment!");

            } else if (cInput == 'C') { // let user check which appointments are available
                String strYear = "";
                bRepeat = true;
                //while user wrong inputs
                while (bRepeat) {
                    try {
                        System.out.print("Enter Year (numbers>=0): ");
                        strYear = in.next();
                        if (Integer.parseInt(strYear) < 0) { //repeat if user enters negative year
                            System.out.println("Year can't be smaller than 0");
                            continue;
                        }
                        bRepeat = false;
                    } catch (NumberFormatException e) { //if user entered any other value than integer
                        //an array list to store wrong inputs
                        ArrayList<Character> arrChars = new ArrayList<Character>();
                        arrChars = CheckValid(strYear);
                        //if List is not empty
                        if (!arrChars.isEmpty()) {
                            System.out.print("Please dont enter: ");
                            //print list
                            for (char cTemp : arrChars) {
                                System.out.print(String.format(" %c", cTemp));
                            }
                            System.out.println("");
                        }
                        in.nextLine();
                    }
                }
                //asks user to enter day
                int nDay = CheckDay();
                //asks user to enter month
                int nMonth = CheckMonth();
                //if there are appointments
                if(!appLists.isEmpty()) {
                    //check if appointments fit the dates
                    for (int i = 0; i < appLists.size(); i++) {
                        if (appLists.get(i).occursOn(Integer.parseInt(strYear), nMonth, nDay) == true) {
                            System.out.println(appLists.get(i).getAppointment());
                        }
                    }
                    System.out.println("Checking finished");
                } //if there aren't any appointments
                else {
                    System.out.println("No Appointment set");
                }
            } else if (cInput == 'Q') { //end program if user enters Q
                System.out.println("End");
            } else{ //if user enter wrong choices
                System.out.println("Please enter correct choice");
            }
        }
    }
}




