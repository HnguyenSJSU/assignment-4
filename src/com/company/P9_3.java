package com.company;
/**
 * Hung Nguyen
 * 013626210
 * This program creates an array of appointments of different types and
 * asks user to enter the dates for the appointments
 */

import java.util.ArrayList;

import java.util.Scanner;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * superclass to create appointments
 */
class Appointment{
    //store appointment info
    private int mDate, mMonth, mYear;
    //store appointment description
    private String mDescription;

    //constructor for appointments
    public Appointment(int nYear, int nMonth, int nDay, String tempDes){
        this.mDate = nDay;
        this.mMonth = nMonth;
        this.mYear = nYear;
        this.mDescription = tempDes;
    }

    /**
     * method to check if appointment occurs on date
     * @param year
     * @param month
     * @param day
     * @return true/false
     */
    public boolean occursOn(int year, int month, int day) {
        if ((this.mDate == day) && (this.mMonth == month) && (this.mYear == year)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * setter for date
     * @param nDate
     */
    public void setmDate(int nDate) {
        this.mDate = nDate;
    }

    /**
     * getter for date
     * @return mDate
     */
    public int getmDate() {
        return this.mDate;
    }

    /**
     * setter for month
     * @param nMonth
     */
    public void setmMonth(int nMonth){
        this.mMonth = nMonth;
    }

    /**
     * getter for month
     * @return mMonth
     */
    public int getmMonth(){
        return this.mMonth;
    }

    /**
     * setter for year
     * @param nYear
     */
    public void setmYear(int nYear){
        this.mYear = nYear;
    }

    /**
     * getter for year
     * @return mYear
     */
    public int getmYear(){
        return this.mYear;
    }

    /**
     * setter for description
     * @param strDescription
     */
    public void setmDescription(String strDescription){
        this.mDescription = strDescription;
    }

    /**
     * return description
     * @param strDescription
     * @return this.mDescription
     */
    public String getmDescription(String strDescription){
        return this.mDescription;
    }

    /**
     * getter for appointment
     * @return strReturn
     */
    public String getAppointment() {
        String strReturn = this.mDescription + " - " + this.mDate + "/" + this.mMonth + "/" + this.mYear;
        return strReturn;
    }

}

/**
 * Inherits appointment as a one time appointment
 */
class Onetime extends Appointment {
    //passes user value to Appointment
    public Onetime(int nYear, int nMonth, int nDay, String tempDes) {
        super(nYear, nMonth, nDay, tempDes);
    }

    /**
     * override method from Appointment
     * @return strOneTime
     */
    @Override
    public String getAppointment() {
        String strOneTime = "oneTime appoitnment: " + super.getAppointment();
        return strOneTime;
    }
}

/**
 * inherits Appointment as daily appointment
 */
class Daily extends Appointment {
    //passes user input to Daily
    public Daily(int nYear, int nMonth, int nDay, String tempDes) {
        super(nYear, nMonth, nDay, tempDes);
    }

    @Override
    /**
     * overrides occursOn and check if input is the same or after
     * appointment
     * @param year,month,day
     * @return bReturn
     */
    public boolean occursOn(int year, int month, int day) {
        boolean bReturn = false;
        //create instances of LocalDateTime to compare time
        LocalDateTime a = LocalDateTime.of(year, month, day, 12, 00);
        LocalDateTime b = LocalDateTime.of(this.getmYear(), this.getmMonth(), this.getmDate(), 12, 00);
        //if userInput > daily or if equal date
        if(a.isAfter(b) || a.isEqual(b)){
            bReturn = true;
        }
        else{
            return false;
        }

//        LocalDateTime b = LocalDateTime.of(2012, 7, 1, 12, 00);
//        if ((this.getmDate() == day) && (this.getmMonth() == month) || (this.getmYear() == year)
//        ||(this.getmDate() <=day && this.getmYear() < year && this.getmMonth() <= month)
//        || ((this.getmMonth()> month && this.getmYear() < year && this.getmDate() > day))) {
//            return true;
//        } else {
//            return false;
//        }
        return bReturn;
    }
    @Override
    /**
     * overirdes getAppointment and return daily appointment string
     * @return strDaily
     */
    public String getAppointment() {
        String strDaily = "Daily appoitnment:  " + super.getAppointment();
        return strDaily;

    }
}

/**
 * extends Appointment as monthly appointment
 */
class Monthly extends Appointment{
    public Monthly(int nYear, int nMonth, int nDay, String tempDes) {
        super(nYear,nMonth,nDay,tempDes);
    }

    /**
     * Check if monthly is the same day as user input
     * @param year
     * @param month
     * @param day
     * @return occursOn
     */
    @Override
    public boolean occursOn(int year, int month, int day) {
        if ((this.getmMonth() <= month && this.getmDate()== day && this.getmYear() <= year)
            ||(this.getmMonth()>= month && this.getmDate() == day && this.getmYear() < year )){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return monthly appointment string
     * @return strMonthly
     */
    @Override
    public String getAppointment() {
        String strMonthly = "Monthly appoitnment: " + super.getAppointment();
        return strMonthly;
    }
}

/**
 * asks user to enter appointment
 */
public class P9_3 {
    public static final Scanner in = new Scanner(System.in);

    /**
     * check if string has letters/special characters
     * @param strTemp
     * @return ArrayList
     */
    public static ArrayList<Character> CheckValid(String strTemp) {
        ArrayList<Character> arrChars = new ArrayList<Character>();
        boolean bRep = true;
        boolean flag = true;

        //loop through string to see if it has illegal inputs
        for (int i = 0; i < strTemp.length(); i++) {
            flag = Character.isDigit(strTemp.charAt(i));
            if (flag == true) {//if index is an integer
                i++;
            } else { //else add character into list
                arrChars.add(strTemp.charAt(i));
            }
        }
        return arrChars;
    }

    /**
     * Check if user input of day is valid
     * @return nDay
     */
     public static int CheckDay() {
         int nDay = 0;
         int nCount = 0;
         //create a pattern instance
         Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
         //check if input is valid
         do {
            if (nCount >= 1) {

                System.out.println(String.format("%s is not valid", nDay));
            }
            System.out.print("Enter day: ");
            //while userinput is wrong
            while (!in.hasNextInt()) {
                String strTemp = in.next();
                Matcher m = p.matcher(strTemp);
                boolean b = m.find();
                //if input is invalid
                if (b) {
                    System.out.println(String.format("Special character %s is not valid", strTemp));
                    System.out.print("Enter day: ");
                } //if there is no special character
                else {
                    System.out.println(String.format("Letters are not valid: %s", strTemp));
                    System.out.print("Enter day: ");
                }
            }
            nDay = in.nextInt();
            nCount++;
        } while (nDay < 0 || nDay > 32);

         return nDay;

     }

    /**
     * Checking if user input of month is valid
     * @return nMonth
     */
     public static int CheckMonth() {
        int nCount = 0;
        int nMonth = 0;
        //create pattern instance
        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        //check if user input of month is valid
         do {
            if (nCount >= 1) {
                System.out.println(String.format("%s is not valid", nMonth));
            }
            System.out.print("Enter Month: ");
            //while user input is invalid
            while (!in.hasNextInt()) {
                String strTemp = in.next();
                Matcher m = p.matcher(strTemp);
                boolean b = m.find();
                //if input contains special characters
                if (b) {
                    System.out.println(String.format("Special character %s is not valid", strTemp));
                    System.out.print("Enter Month: ");
                } else { //if input contains letters
                    System.out.println(String.format("Letters are not valid: %s", strTemp));
                    System.out.print("Enter Month: ");
                }
            }
            nMonth = in.nextInt();
            nCount++;
        } while (nMonth < 0 || nMonth > 12);

         return nMonth;

     }

    /**
     * asks for user input
     * @param args
     */
    public static void main(String[] args) {

        //create a list of appointments
        Appointment[] appLists = {
            new Monthly(2019, 10, 01, "Go to dentist"),
            new Daily(2019, 12, 30, "Go visit"),
            new Daily(2020, 12, 30, "Go play"),
            new Onetime(2022, 8, 20, "Go hangout"),
            new Monthly(2021, 10, 25, "Go to market"),
        };
        boolean bRepeat = true;
        String strYear = "";
        //checking if year input is valid
        while (bRepeat) {
            try {
                System.out.print("Enter Year (numbers>0): ");
                strYear = in.next();
                if (Integer.parseInt(strYear) < 0) {
                    System.out.println("Year can't be smaller than 0");
                    continue;
                }
                bRepeat = false;
            } catch (NumberFormatException e) { //if user entered any other value than integer
                ArrayList<Character> arrChars = new ArrayList<Character>();
                arrChars = CheckValid(strYear);
                //if List is not empty
                if (!arrChars.isEmpty()) {
                    System.out.print("Please dont enter: ");
                    //print list
                    for (char cTemp : arrChars) {
                        System.out.print(String.format(" %c", cTemp));
                    }
                    System.out.println("");
                }
                in.nextLine();
            }
        }
        int nDay = 0;
        int nCount = 0;
        //ask user for day
        nDay = CheckDay();
        //ask month from user
        int nMonth = CheckMonth();
        //print list of appointments if met appointment
        for(int i = 0; i<appLists.length; i++ ){
            if(appLists[i].occursOn(Integer.parseInt(strYear), nMonth,nDay)== true){
                System.out.println(appLists[i].getAppointment());
            }
        }
        System.out.println("Checking finished");

    }
}
/*
Enter Year (numbers>0): -1
Year can't be smaller than 0
Enter Year (numbers>0): 2020
Enter day: a-
Special character a- is not valid
Enter day: 01
Enter Month: a
Letters are not valid: a
Enter Month: 01
Monthly appoitnment: Go to dentist - 1/10/2019
Daily appoitnment:  Go to work - 30/12/2019
Checking finished

Process finished with exit code 0

 */



