/**
 * Hung Nguyen
 * id: 013626210
 * This program creates a BetterRectangle class that inherits from Rectangle
 * and calculate area and perimeters of rectangle.
 */
package com.company;
import java.awt.Rectangle;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * BetterRectangle is a class that inherits Rectangle to use
 */
class BetterRectangle extends Rectangle {
    /**
     * BetterRectangle constructor takes in axis and fields of rectangle
     * @param nXAxis
     * @param nYAxis
     * @param nWidth
     * @param nHeight
     *
     */
    public BetterRectangle(int nXAxis, int nYAxis, int nWidth, int nHeight) {
        //calls Rectangle methods
        super.setLocation(nXAxis,nYAxis);
        super.setSize(nWidth,nHeight);
    }

    /**
     * return perimeter of rectangle
     * @return this.getWidth() *2 + this.getHeight()*2
     */
    public double getPerimeter() {
        //call Rectangle methods to find perimeters
        return this.getWidth() *2 + this.getHeight()*2;
    }

    /**
     * return area of rectangle
     * @return this.getWidth() * this.getHeight()
     */
    public double getArea() {
        //call Rectangle methods to find area
        return this.getWidth() * this.getHeight();
    }
}

/**
 * asks user to set fields for the rectangle
 */
public class E9_13 {

    public static void main(String[] args) {
        //set Scanner class
        Scanner in = new Scanner(System.in);
        //declare variables for userInput
        int nInputX, nInputY, nInputWidth, nInputHeight;
        nInputX = nInputY = nInputWidth = nInputHeight = 0;
        //set flag
        boolean bRep = true;
        //while input is wrong
        while(bRep == true) {
            try {

                System.out.print("Enter x: ");
                nInputX = in.nextInt();
                System.out.print("Enter y: ");
                nInputY = in.nextInt();
                if(nInputX < 0 || nInputY < 0) {
                    System.out.println("x or y can't be negative ");
                    continue;
                }

                System.out.print("Enter height: ");
                nInputHeight = in.nextInt();
                System.out.print("Enter width: ");
                nInputWidth = in.nextInt();
                if(nInputHeight < 0 || nInputWidth < 0) {
                    System.out.println("Height or Width can't be negative ");
                    continue;
                }

                bRep = false;

            }catch(InputMismatchException e) { //if user entered wrong values
                System.out.println("Letters/Characters are not accepted");
                in.nextLine();
            }
        }
        //create BetterRectangle object
        BetterRectangle Rectangle1 = new BetterRectangle(nInputX,nInputY,nInputWidth,nInputHeight);

        //prints perimeter and area
        System.out.println(String.format("Perimiter: %s ", Rectangle1.getPerimeter()));
        System.out.println(String.format("Area: %s", Rectangle1.getArea()));
    }

}
/*
Enter x: 0
Enter y: 0
Enter height: 10
Enter width: 10
Perimiter: 40.0
Area: 100.0

Process finished with exit code 0
 */